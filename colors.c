/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/12 04:47:28 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/19 11:44:17 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	set_color1(t_f *data)
{
	data->colors[0][0] = 0x03164F;
	data->colors[0][1] = 0x19071A;
	data->colors[0][2] = 0x09012F;
	data->colors[0][3] = 0x040449;
	data->colors[0][4] = 0x000764;
	data->colors[0][5] = 0x0C2C8A;
	data->colors[0][6] = 0x1852B1;
	data->colors[0][7] = 0x397DD1;
	data->colors[0][8] = 0x86B5E5;
	data->colors[0][9] = 0xD3ECF8;
	data->colors[0][10] = 0xF1E9BF;
	data->colors[0][11] = 0xF8C95B;
	data->colors[0][12] = 0xFFAA00;
	data->colors[0][13] = 0xCC8000;
	data->colors[0][14] = 0x995700;
	data->colors[0][15] = 0x6A3403;
}

static void	set_color2(t_f *data)
{
	data->colors[1][0] = 0x153F17;
	data->colors[1][1] = 0x174719;
	data->colors[1][2] = 0x195B1D;
	data->colors[1][3] = 0x1C8221;
	data->colors[1][4] = 0x22AA29;
	data->colors[1][5] = 0x23D12C;
	data->colors[1][6] = 0x2AEDA2;
	data->colors[1][7] = 0x41E2EA;
	data->colors[1][8] = 0x32CBD3;
	data->colors[1][9] = 0x1B9CA3;
	data->colors[1][10] = 0x187C82;
	data->colors[1][11] = 0x116168;
	data->colors[1][12] = 0x114768;
	data->colors[1][13] = 0x104368;
	data->colors[1][14] = 0x125C84;
	data->colors[1][15] = 0x0E4363;
}

static void	set_color3(t_f *data)
{
	data->colors[2][0] = 0x000777;
	data->colors[2][1] = 0x0B16AA;
	data->colors[2][2] = 0x8187DB;
	data->colors[2][3] = 0x4C0FDB;
	data->colors[2][4] = 0x120FDB;
	data->colors[2][5] = 0x1815C4;
	data->colors[2][6] = 0x201E99;
	data->colors[2][7] = 0x45449B;
	data->colors[2][8] = 0x4542F4;
	data->colors[2][9] = 0x70ABCC;
	data->colors[2][10] = 0x70B1CC;
	data->colors[2][11] = 0x70A4CC;
	data->colors[2][12] = 0x65A9CE;
	data->colors[2][13] = 0x509BC4;
	data->colors[2][14] = 0x3A38B7;
	data->colors[2][15] = 0x1A238C;
}

static void	set_color4(t_f *data)
{
	data->colors[3][0] = 0x024C13;
	data->colors[3][1] = 0x113D68;
	data->colors[3][2] = 0x09012F;
	data->colors[3][3] = 0x040449;
	data->colors[3][4] = 0x000764;
	data->colors[3][5] = 0x0C2C8A;
	data->colors[3][6] = 0x1852B1;
	data->colors[3][7] = 0x397DD1;
	data->colors[3][8] = 0x86B5E5;
	data->colors[3][9] = 0xD3ECF8;
	data->colors[3][10] = 0xF1E9BF;
	data->colors[3][11] = 0x8BDB3B;
	data->colors[3][12] = 0x18A580;
	data->colors[3][13] = 0x085470;
	data->colors[3][14] = 0x213C60;
	data->colors[3][15] = 0x013F42;
}

void		color_palette(t_f *data)
{
	set_color1(data);
	set_color2(data);
	set_color3(data);
	set_color4(data);
	data->colors[4][0] = 0x021021;
	data->colors[4][1] = 0x042042;
	data->colors[4][2] = 0x121212;
	data->colors[4][3] = 0x242424;
	data->colors[4][4] = 0x424242;
	data->colors[4][5] = 0x777777;
	data->colors[4][6] = 0x888888;
	data->colors[4][7] = 0x999999;
	data->colors[4][8] = 0x2A2A2A;
	data->colors[4][9] = 0x4B4B4B;
	data->colors[4][10] = 0xBBBBBB;
	data->colors[4][11] = 0xCCCCCC;
	data->colors[4][12] = 0xDCDCDC;
	data->colors[4][13] = 0xE4E4E4;
	data->colors[4][14] = 0xEEEEEE;
	data->colors[4][15] = 0xF4F4F4;
}
