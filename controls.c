/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controls.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 06:16:54 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/27 16:26:04 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	drop_the_bass(t_f *data)
{
	if (!(data->music = fork()))
	{
		usleep(42000);
		execl("/usr/bin/afplay", "afplay", "morepower.mp3", NULL);
		exit(0);
	}
}

int		controls(int key, t_f *data)
{
	if (key == KEY_OPEN_BRACKET || key == KEY_CLOSE_BRACKET)
		change_iter_nb(data, key);
	else if (is_arrow_key(key))
		move(data, key);
	else if (key == KEY_O)
		data->palette = ((data->palette + 1) % PALETTES);
	else if (key == KEY_P)
		data->pattern = !data->pattern;
	else if (key == KEY_K)
	{
		cycle_colors(data);
		if (!data->music)
			drop_the_bass(data);
	}
	else if (key == KEY_MINUS || key == KEY_EQUAL)
		data->zoom_lvl *= (key == KEY_MINUS ? 0.5 : 2);
	else if (key == KEY_L)
		data->lock = !data->lock;
	else if (key == KEY_ESC)
	{
		free_mem(&data);
		exit(0);
	}
	render(data);
	return (0);
}

int		mousectrl(int key, int x, int y, t_f *data)
{
	if (key == MOUSE_SCROLL_UP)
		data->scale -= data->zoom_lvl * data->scale;
	else if (key == MOUSE_SCROLL_DOWN)
		data->scale += data->zoom_lvl * data->scale;
	else if (key == 1)
		recenter(x, y, data);
	render(data);
	return (0);
}

int		juliaswitch(int x, int y, t_f *data)
{
	if (!data->lock)
	{
		if (!ft_strncmp(data->fractal_name, "julia", 5) && ft_strcmp(
		data->fractal_name, "julia") && ft_strcmp(data->fractal_name, "julia2"))
			data->max_iterations = 32;
		data->cx = data->xoff + ((x - ((double)data->img_x / 2.0))
		/ ((double)data->img_x / 2.0)) * data->scale / 2.0;
		data->cy = data->yoff - ((y - ((double)data->img_y / 2.0))
		/ ((double)data->img_y / 2.0)) * data->scale / 2.0;
		render(data);
	}
	return (0);
}

int		stop_music(int key, t_f *data)
{
	if (key == KEY_K && data->music)
	{
		kill(data->music, SIGTERM);
		data->music = 0;
	}
	return (0);
}
