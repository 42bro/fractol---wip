/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/12 04:38:07 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/27 15:30:03 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		is_used_key(char c)
{
	return (c == KEY_ESC);
}

int		is_arrow_key(char c)
{
	return (c == KEY_UP || c == KEY_DOWN || c == KEY_LEFT || c == KEY_RIGHT);
}

void	ft_swap(int *a, int *b)
{
	int	t;

	t = *a;
	*a = *b;
	*b = t;
}

int		is_valid_set(char *str)
{
	return (!(ft_strcmp(str, "mandelbrot") && ft_strcmp(str, "julia")
	&& ft_strcmp(str, "tricorn") && ft_strcmp(str, "julia2")
	&& ft_strcmp(str, "julia3") && ft_strcmp(str, "julia4")
	&& ft_strcmp(str, "julia5") && ft_strcmp(str, "julia6")
	&& ft_strcmp(str, "julia7") && ft_strcmp(str, "ship")
	&& ft_strcmp(str, "ship2") && ft_strcmp(str, "ship3")
	&& ft_strcmp(str, "ship4")));
}

int		invalid_args(int ac, char **av)
{
	return (ac != 2 || !is_valid_set(av[1]));
}
