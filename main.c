/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/05 15:35:39 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/27 16:20:18 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	start_hooks(t_f *data)
{
	mlx_hook(data->win_ptr, MOUSE_CLICK, MOUSE_CLICK, mousectrl, data);
	if (!ft_strncmp(data->fractal_name, "julia", 5))
		mlx_hook(data->win_ptr, MOUSE_MOTION, MOUSE_MOTION_MASK, juliaswitch,
	data);
	mlx_hook(data->win_ptr, KEY_RELEASE, KEY_RELEASE_MASK, stop_music, data);
	mlx_hook(data->win_ptr, KEY_PRESS, KEY_PRESS_MASK, controls, data);
}

void		usage_msg(void)
{
	ft_putendl("usage : ./fractol <set>\n\navailable sets :\n\n- mandelbrot\n- \
ship\n- ship2\n- ship3\n- ship4\n- julia\n- julia2 (modded z² + c)\n- julia3 \
(z³ + c)\n- julia4 (z⁴ + c)\n- julia5 (z⁵ + c)\n- julia6 (z⁶ + c)\n- julia7 \
(z⁷ + c)\n- tricorn\n\ncontrols :\n- move : mouse click / arrow keys\n- zoom : \
scroll wheel\n- less/more zoom : - +\n\
- less/more iterations : [  ]\n- lock fractal (julia sets only) : L\n- switch \
color pattern : O\n- switch color mode (iterative / modulo) : P\n- cycle color\
s : tap K\n- \033[34;103ma c i d   m o d e :\033[0;0m hold K ");
	exit(1);
}

int			main(int ac, char **av)
{
	t_f	*data;
	int	bpp;
	int	linesize;
	int endian;

	if (invalid_args(ac, av))
		usage_msg();
	bpp = 32;
	linesize = ((bpp / 4) * IMG_X);
	endian = 1;
	data = malloc_de_ouf();
	start_mlx(data);
	data->gsize = data->img_x * data->img_y;
	data->fractal_name = av[1];
	data->max_iterations = (!ft_strncmp(data->fractal_name, "julia",
	5) ? MIN_ITER : data->max_iterations);
	data->pix_array = (int*)mlx_get_data_addr(data->img_ptr, &bpp, &linesize,
	&endian);
	start_opencl(data);
	render(data);
	start_hooks(data);
	mlx_loop(data->mlx_ptr);
	free_mem(&data);
	return (0);
}
