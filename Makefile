# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fdubois <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/18 08:48:23 by fdubois           #+#    #+#              #
#    Updated: 2019/02/27 16:32:00 by fdubois          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
FLAGS = -Wall -Wextra -Werror
SRC = colors.c controls.c mem.c moves.c render.c utils.c main.c
HDR = fractol.h
LIBDIR = libft/
MLXDIR = mlx/
OBJ = $(SRC:.c=.o)
NAME = fractol

.PHONY: all libs clean fclean re

all: libs $(NAME)

libs:
	make -C $(LIBDIR)
	make -C $(MLXDIR)

$(NAME): $(OBJ)
	$(CC) $(FLAGS) $(OBJ) -L $(LIBDIR) -lft -L $(MLXDIR) -lmlx -framework OpenGL -framework AppKit -framework OpenCL -o $(NAME)

%.o: %.c $(HDR)
	$(CC) $(FLAGS) -I. -o $@ -c $<

clean:
	make -C $(LIBDIR) clean
	make -C $(MLXDIR) clean
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)
	rm -rf $(LIBDIR)libft.a

re: fclean all
