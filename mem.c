/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mem.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 06:00:33 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/27 16:20:36 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	malloc_fail(t_f *data)
{
	ft_putendl("malloc sucks !");
	if (data)
		ft_memdel((void**)&data);
	exit(1);
}

t_f		*malloc_de_ouf(void)
{
	t_f	*data;
	int i;
	int fd;

	i = -1;
	(!(data = (t_f*)malloc(sizeof(t_f))) ? malloc_fail(data) : 1);
	ft_bzero(data, sizeof(*data));
	(!(data->kernel = ft_strnew(BUFF_SIZE)) ? malloc_fail(data) : 1);
	fd = open("kernel.cl", O_RDONLY);
	(read(fd, data->kernel, BUFF_SIZE) < 0 ? exit(1) : close(fd));
	data->img_x = IMG_X;
	data->img_y = IMG_Y;
	if (!(data->colors = (int**)malloc(sizeof(int*) * PALETTES)))
		malloc_fail(data);
	while (++i < PALETTES)
		if (!(data->colors[i] = (int*)malloc(sizeof(int) * 16)))
			malloc_fail(data);
	data->max_iterations = 64;
	data->xmin = -2.0;
	data->ymin = -2.0;
	data->scale = 4.0;
	data->zoom_lvl = 0.33;
	color_palette(data);
	return (data);
}

void	start_mlx(t_f *data)
{
	data->mlx_ptr = mlx_init();
	data->win_ptr = mlx_new_window(data->mlx_ptr, data->img_x, data->img_y,
	"fract'ol");
	data->img_ptr = mlx_new_image(data->mlx_ptr, data->img_x, data->img_y);
}

void	start_opencl(t_f *data)
{
	clGetDeviceIDs(NULL, CL_DEVICE_TYPE_GPU, 1, &(data->cl.device), NULL);
	data->cl.context = clCreateContext(NULL, 1, &(data->cl.device), NULL, NULL,
	NULL);
	data->cl.queue = clCreateCommandQueue(data->cl.context, data->cl.device, 0,
	NULL);
	data->cl.program = clCreateProgramWithSource(data->cl.context, 1,
	(const char **)&data->kernel, NULL, NULL);
	clBuildProgram(data->cl.program, 0, NULL, NULL, NULL, NULL);
	data->cl.kernel = clCreateKernel(data->cl.program, data->fractal_name,
	NULL);
	data->cl.buf_pixels = clCreateBuffer(data->cl.context, CL_MEM_WRITE_ONLY,
	(sizeof(int) * data->img_x * data->img_y), NULL, NULL);
	data->cl.buf_colors = clCreateBuffer(data->cl.context, CL_MEM_READ_ONLY,
	(sizeof(int) * 16), NULL, NULL);
	data->cl.buf_params = clCreateBuffer(data->cl.context, CL_MEM_READ_ONLY,
	sizeof(t_params), NULL, NULL);
	clSetKernelArg(data->cl.kernel, 0, sizeof(cl_mem), &(data->cl.buf_pixels));
	clSetKernelArg(data->cl.kernel, 1, sizeof(cl_mem), &(data->cl.buf_colors));
	clSetKernelArg(data->cl.kernel, 2, sizeof(cl_mem), &(data->cl.buf_params));
}

void	free_mem(t_f **data)
{
	int i;

	i = -1;
	clReleaseMemObject((*data)->cl.buf_pixels);
	clReleaseMemObject((*data)->cl.buf_colors);
	clReleaseMemObject((*data)->cl.buf_params);
	clReleaseKernel((*data)->cl.kernel);
	clReleaseProgram((*data)->cl.program);
	clReleaseCommandQueue((*data)->cl.queue);
	clReleaseContext((*data)->cl.context);
	clReleaseDevice((*data)->cl.device);
	while (++i < PALETTES)
		free((*data)->colors[i]);
	free((*data)->colors);
	free((*data)->kernel);
	mlx_destroy_image((*data)->mlx_ptr, (*data)->img_ptr);
	mlx_destroy_window((*data)->mlx_ptr, (*data)->win_ptr);
	free(*data);
}
