/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moves.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/12 04:38:25 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/27 14:05:46 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	move(t_f *data, int key)
{
	if (key == KEY_UP)
		data->yoff += data->scale / 8.0;
	else if (key == KEY_DOWN)
		data->yoff -= data->scale / 8.0;
	else if (key == KEY_LEFT)
		data->xoff -= data->scale / 8.0;
	else if (key == KEY_RIGHT)
		data->xoff += data->scale / 8.0;
}

void	change_iter_nb(t_f *data, int key)
{
	data->max_iterations *= (key == KEY_CLOSE_BRACKET && data->max_iterations
	< MAX_ITER ? 2 : 1);
	data->max_iterations /= (key == KEY_OPEN_BRACKET && data->max_iterations
	> MIN_ITER ? 2 : 1);
}

void	recenter(int x, int y, t_f *data)
{
	data->xoff += (((double)x - ((double)data->img_x / 2))
	/ ((double)data->img_x / 2)) * (data->scale / 2.0);
	data->yoff += -(((double)y - ((double)data->img_y / 2))
	/ ((double)data->img_y / 2)) * (data->scale / 2.0);
}

void	cycle_colors(t_f *data)
{
	int	i;

	i = -1;
	while (++i < 15)
		ft_swap(&data->colors[data->palette][i],
	&data->colors[data->palette][i + 1]);
}
