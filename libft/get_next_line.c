/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 19:31:18 by fdubois           #+#    #+#             */
/*   Updated: 2018/12/29 07:18:44 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	getfile(const int fd, char **line, char **file)
{
	char	*str;

	str = *line;
	*line = ft_strnjoin(str, file[fd], ft_strlen(file[fd]) -
			ft_strlen(ft_strchr(file[fd], '\n')));
	free(str);
	str = file[fd];
	file[fd] = ft_strdup(ft_strchr(file[fd], '\n'));
	free(str);
	if (file[fd])
	{
		str = file[fd];
		file[fd] = ft_strdup(file[fd] + 1);
		free(str);
		return (1);
	}
	return (0);
}

int			get_next_line(const int fd, char **line)
{
	static char	*file[4865];
	int			readval;

	if (fd < 0 || fd > 4864 || line == NULL || !(*line = ft_strnew(0)))
		return (-1);
	readval = 1;
	while (readval != -1)
	{
		if (file[fd] == NULL)
		{
			file[fd] = ft_strnew(BUFF_SIZE);
			if ((readval = read(fd, file[fd], BUFF_SIZE)) == -1)
				return (-1);
		}
		if (getfile(fd, line, file))
			return (1);
		if (readval == 0)
		{
			if (*line[0] == '\0')
				return (0);
			return (1);
		}
	}
	return (-1);
}
