/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 16:00:05 by fdubois           #+#    #+#             */
/*   Updated: 2018/12/03 13:06:34 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnjoin(char *s1, char *s2, size_t n)
{
	size_t	i;
	size_t	j;
	char	*ret;

	i = 0;
	j = 0;
	ret = ft_strnew(ft_strlen(s1) + n + 1);
	while (s1[i])
	{
		ret[i] = s1[i];
		i++;
	}
	while (s2[j] && j < n)
		ret[i++] = s2[j++];
	ret[i] = '\0';
	return (ret);
}
