/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/03 14:47:15 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/27 16:31:42 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include "libft/libft.h"
# include <math.h>
# include <fcntl.h>
# include <signal.h>
# include <time.h>
# include <OpenCL/cl.h>
# include "mlx/mlx.h"

# define IMG_X				1680
# define IMG_Y				1050
# define PALETTES			5
# define BUFF_SIZE			16384
# define MIN_ITER			16
# define MAX_ITER			1024

# define KEY_ESC			53
# define KEY_MINUS			27
# define KEY_EQUAL			24
# define KEY_O				31
# define KEY_P				35
# define KEY_K				40
# define KEY_L				37
# define KEY_OPEN_BRACKET	33
# define KEY_CLOSE_BRACKET	30
# define KEY_LEFT			123
# define KEY_DOWN			125
# define KEY_RIGHT			124
# define KEY_UP				126
# define KEY_PAGE_UP		116
# define KEY_PAGE_DOWN		121

# define KEY_PRESS_MASK		(1L << 0)
# define KEY_PRESS			(1L << 1)
# define KEY_RELEASE_MASK	(1L << 1)
# define KEY_RELEASE		3
# define MOUSE_MOTION_MASK	(1L << 6)
# define MOUSE_MOTION		6
# define MOUSE_CLICK		(1L << 2)
# define MOUSE_SCROLL_UP	4
# define MOUSE_SCROLL_DOWN	5

typedef struct			s_cl
{
	cl_device_id		device;
	cl_context			context;
	cl_command_queue	queue;
	cl_program			program;
	cl_kernel			kernel;
	cl_mem				buf_pixels;
	cl_mem				buf_colors;
	cl_mem				buf_params;
}						t_cl;

typedef struct			s_params
{
	double				xmin;
	double				ymin;
	double				xoff;
	double				yoff;
	double				scale;
	double				cx;
	double				cy;
	size_t				max_iterations;
	size_t				imgx;
	size_t				imgy;
	int					palette;
	char				pattern;
}						t_params;

typedef struct			s_f
{
	int					**colors;
	void				*mlx_ptr;
	void				*win_ptr;
	void				*img_ptr;
	t_cl				cl;
	int					*pix_array;
	char				*kernel;
	char				*fractal_name;
	double				xmin;
	double				ymin;
	double				xoff;
	double				yoff;
	double				scale;
	double				cy;
	double				cx;
	double				zoom_lvl;
	size_t				max_iterations;
	size_t				img_x;
	size_t				img_y;
	size_t				gsize;
	pid_t				music;
	int					palette;
	char				pattern;
	char				lock;
}						t_f;

void					render(t_f *data);
int						is_used_key(char c);
int						is_arrow_key(char c);
t_f						*malloc_de_ouf(void);
void					drop_the_bass(t_f *data);
void					start_mlx(t_f *data);
void					start_opencl(t_f *data);
void					free_mem(t_f **data);
int						is_valid_set(char *str);
int						invalid_args(int ac, char **av);
void					color_palette(t_f *data);
void					cycle_colors(t_f *data);
void					switch_pattern(t_f *data);
void					switch_palette(t_f *data);
void					move(t_f *data, int key);
void					change_iter_nb(t_f *data, int key);
void					recenter(int x, int y, t_f *data);
int						controls(int key, t_f *data);
int						mousectrl(int key, int x, int y, t_f *data);
int						juliaswitch(int x, int y, t_f *data);
int						stop_music(int key, t_f *data);
void					ft_swap(int *a, int *b);

#endif
