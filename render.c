/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 06:14:07 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/27 15:26:10 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_params	kernel_data(t_f *data)
{
	t_params	ret;

	ret.max_iterations = data->max_iterations;
	ret.xmin = data->xmin;
	ret.ymin = data->ymin;
	ret.xoff = data->xoff;
	ret.yoff = data->yoff;
	ret.imgx = data->img_x;
	ret.imgy = data->img_y;
	ret.scale = data->scale;
	ret.cx = data->cx;
	ret.cy = data->cy;
	ret.pattern = data->pattern;
	ret.palette = data->palette;
	return (ret);
}

void		render(t_f *data)
{
	t_params	lite;
	size_t		global_size;

	global_size = data->img_x * data->img_y;
	lite = kernel_data(data);
	clEnqueueWriteBuffer(data->cl.queue, data->cl.buf_colors, CL_FALSE, 0,
	(sizeof(int) * 16), data->colors[data->palette], 0, NULL, NULL);
	clEnqueueWriteBuffer(data->cl.queue, data->cl.buf_params, CL_FALSE, 0,
	sizeof(lite), &lite, 0, NULL, NULL);
	clEnqueueNDRangeKernel(data->cl.queue, data->cl.kernel, 1, NULL,
	&(data->gsize), NULL, 0, NULL, NULL);
	clEnqueueReadBuffer(data->cl.queue, data->cl.buf_pixels, CL_FALSE, 0,
	sizeof(int) * global_size, data->pix_array, 0, NULL, NULL);
	clFlush(data->cl.queue);
	clFinish(data->cl.queue);
	mlx_put_image_to_window(data->mlx_ptr, data->win_ptr, data->img_ptr, 0, 0);
}
